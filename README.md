# taskwarrior-uuid-numbering

Released under MIT license.

This is free software; see source for copying conditions. There is no warranty; not even for MERCHANTABILIY or FITNESS FOR A PARTICULAR PURPOSE.

-------------------------------------------------------

Name

> taskwarrior-uuid-numbering - Set numbering in "task" command, taskwarrior.

Synopsis

> taskwarrior-uuid-numbering [completed | all | deleted]

Description

> Place 'taskwarrior-uuid-numbering' and 'regex_check_input.pl' under '/usr/local/bin'

> Set numbering of "task" command (taskwarrior), for its 'completed', 'deleted' or 'all' that only provides longer and non-intuitive UUID like 'bb8f8106'. This tool will help user to specify UUID with common numbering style like '1-3,9,16', for multiple UUID(s) specifying support.

> For original 'task' tool:

``` 
BASH:~$ task all # "D" is deleted task / "C" is completed task / "P" is pending

ID St UUID     Age   Done  Project                   Description
 1 P  b5d0ce67 22min                                 one
 2 P  195c93a9 22min                                 two        
 3 P  ddd8970b 22min                                 three
 4 P  8d601640 22min                                 four       
 5 P  f4886bbf 22min       fix_py3_lib_[ported_2to3] eight
 - C  37766a2d 22min 20min                           five       
 - C  c2f819f0 22min 20min fix_py3_lib_[ported_2to3] six
 - C  239ab68b 22min 20min fix_py3_lib_[ported_2to3] seven      
 - D  6fcda6f2 22min 20min fix_py3_lib_[ported_2to3] nine
 - D  ae5271c5 22min 20min fix_py3_lib_[ported_2to3] ten        
 - D  a73d71e3 22min 20min fix_py3_lib_[ported_2to3] eleven
 - D  dcc10a95 22min 20min                           twelve     

12 tasks
```

> For 'taskwarrior-uuid-numbering' tool:

```
BASH:~$ taskwarrior-uuid-numbering all
     1  P  b5d0ce67 23min                                 one
     2  P  195c93a9 23min                                 two
     3  P  ddd8970b 23min                                 three
     4  P  8d601640 23min                                 four
     5  P  f4886bbf 23min       fix_py3_lib_[ported_2to3] eight
     6  C  37766a2d 23min 21min                           five
     7  C  c2f819f0 23min 21min fix_py3_lib_[ported_2to3] six
     8  C  239ab68b 23min 21min fix_py3_lib_[ported_2to3] seven
     9  D  6fcda6f2 23min 21min fix_py3_lib_[ported_2to3] nine
    10  D  ae5271c5 23min 21min fix_py3_lib_[ported_2to3] ten
    11  D  a73d71e3 23min 21min fix_py3_lib_[ported_2to3] eleven
    12  D  dcc10a95 23min 21min                           twelve
Select number without spacing. (Ex: 1-2,5)
Input: 6-7,9,11-12
Write output to file (WARNING: Specified file will be overwritten!)
Filename: file

BASH:~$ cat file
37766a2d c2f819f0 6fcda6f2 a73d71e3 dcc10a95

BASH:~$ task `cat file` modify status:pending
  - Status will be changed from 'completed' to 'pending'.
Modify task 37766a2d 'five'? (yes/no/all/quit) all
Modifying task 37766a2d 'five'.
Modifying task c2f819f0 'six'.
Modifying task 6fcda6f2 'nine'.
Modifying task a73d71e3 'eleven'.
Modifying task dcc10a95 'twelve'.
Modified 5 tasks.
Project 'fix_py3_lib_[ported_2to3]' is 20% complete (4 of 5 tasks remaining).

BASH:~$ task
[task next]

ID Age   Project                   Description Urg 
 5 25min fix_py3_lib_[ported_2to3] eight          1
 7 25min fix_py3_lib_[ported_2to3] six            1
 8 25min fix_py3_lib_[ported_2to3] nine           1
 9 25min fix_py3_lib_[ported_2to3] eleven         1
 1 25min                           one            0
 2 25min                           two            0
 3 25min                           three          0
 4 25min                           four           0
 6 25min                           five           0
10 25min                           twelve         0

10 tasks
```

Input Validation (For Testing)

> Input validation with Perl REGEX engine, against "test.txt" as input:

> `` BASH:~$ (for i in `cat test.txt`;do ./regex_check_input.PL $i;echo " : $i";done)|sort ``
